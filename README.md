# Code Awesome


## Process et cycle de vie

- Agilité

## Versioning

- Git 
- Feature branching

## Intégration Continue / Déploiement Continue

- 

## Test

- End to End
- Integration
- Test unitaire
- Test de composant

## Architecture

- Hexagonal
- CQRS 
- Event sourcing

## Paradigme

- Programmation Orienté Objet
- Programmation Fonctionnelle

## Technologies/Librairie

### Back

### Front

### Infra
